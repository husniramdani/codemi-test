# CODEMI QR CODE TEST
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/overthinking.jpg" height="200" />
</p>

## APK
download di : https://mega.nz/file/IxIThaCI#A9bJwFzCC0Ke7CwlwQ-a0VBRUNYCkMnPuXUnMKxAoSs atau https://drive.google.com/drive/folders/1H15fn7xx1ITseP6A6t_oC1-q_D_Cc_In?usp=sharing

**Login User with**
- email : george@mail.com , passwd : password
- email : janet@mail.com , passwd : password

selain itu email dan password yang salah akan auto masuk sebagai geroge karena api menggunakan fake auth rest api https://reqres.in

## Cara instalasi local
1. Clone Repository ini https://gitlab.com/husniramdani/codemi-test.git
2. install dependencies project dengan ```npm install atau yarn install```
3. Tutorial setup debuging on android device dapat dilihat disini https://reactnative.dev/docs/running-on-device
4. Jika setup sudah selesai jalankan dengan ```react-native run-android``` app akan terinstall dan running di port :8081
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/react-native-run-android.jpg" height="200" />
</p>
5. jangan lupa .env.example ubah menjadi .env

6. Cek ip dengan ipconfig (windows) pada cmd untuk melihat ipv4 address laptop (pastikan satu jaringan sama (wifi) dengan device)
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/ipconfig.jpg" height="200" />
</p>
8. ubah config.js http://192.168.43.150:8000 dengan ipv4 kalian diikuti :8000

9. press d atau shake handphone untuk membuka developer menu
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/yarn-start.jpg" height="200" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/setting.jpg" height="200" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/debug-server.jpg" height="200" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/debug-server-sample.jpg" height="200" />
</p>

10. jika sudah akan menampilkan login page
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/login.jpg" height="500" />
</p>

11. Selanjutnya Clone Repository CODEMI-FAKE AUTH https://gitlab.com/husniramdani/codemi-fake-auth dan ikuti instruksi yang ada untuk menjalankan server

12. kemudian login dengan user sesuai user.js ex : husni@mail.com password, naruto@mail.com password, sasuke@mail.com password

13. setelah berhasil login
<p align="center">
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/home.jpg" height="350" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/profile.jpg" height="350" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/qrcode.jpg" height="350" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/scan-qr-code.jpg" height="350" />
<img src="https://raw.githubusercontent.com/spindyzel/gambar/main/scan-qr-code-result.jpg" height="350" />
</p>

## Alternatif debug apk local server
download debug apk versi local dilink : https://mega.nz/file/IwhkFYYK#B0YkomOBAzj9vD5OhjLeGKL_Ip4S7tnJ8RVHspOYCaU
versi local ini tidak akan bisa melakukan login karena menggunakan default api local saya.