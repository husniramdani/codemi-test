/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {
  Component
} from 'react';
import {
  SafeAreaView,
  StatusBar
} from 'react-native'
import MainRouter from './src/router/MainRouter'
// import Store from './app/store';
import { Provider } from 'react-redux';
import { Store, Persistor } from './src/Store';
import { PersistGate } from 'redux-persist/integration/react';
import { View } from 'react-native-animatable';
import { c, b } from './src/utils/StyleHelper';


export default class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <SafeAreaView style={{ flex: 1 }}>
            <StatusBar barStyle="light-content" />
            <View style={[b.container, c.bgLight,]}>
              <MainRouter />
            </View>
          </SafeAreaView>
        </PersistGate>
      </Provider>
    );
  }
}