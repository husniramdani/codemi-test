import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Svg from '../components/Svg';
import {b, c, p, f} from '../utils/StyleHelper';
import {gradientColor} from '../utils/Color';
import {xs} from '../utils/Responsive';
import {updateConfig} from '../modules/config/ConfigAction';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('AuthIndex');
    }, 1200);
  }
  render() {
    return (
      <>
        <LinearGradient colors={gradientColor} style={s.container}>
          <View
            style={[
              {backgroundColor: 'rgba(255, 255, 255, 0.4)'},
              b.roundedHigh,
              p.center,
              b.shadow
            ]}>
            <Svg icon="logoSplash" size={xs(180)} />
          </View>
          <Text style={s.title}>codemi</Text>
        </LinearGradient>
      </>
    );
  }
}
const s = {
  container: [b.container, p.center],
  title: [f._46, f.gothamBold, b.pt2],
  content: [f._16, f.gothamBold, c.grey1, b.pt2],
};
const mapStateToProps = (state) => ({
  newUser: state.auth.newUser,
});
const mapDispatchToProps = (dispatch) => ({
  updateConfig: () => dispatch(updateConfig()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Splash);
