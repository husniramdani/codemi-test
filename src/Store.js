import { AsyncStorage } from 'react-native'
import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
// import reducers
import RootReducer from './RootReducer'
import thunk from 'redux-thunk'
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2,
    whitelist: ['auth',]
}

const persistedReducer = persistReducer(persistConfig, RootReducer)

const Store = createStore(
    persistedReducer,
    {},
    applyMiddleware(thunk)
)

let Persistor = persistStore(Store)

export {
    Store, Persistor
}