import React, { memo } from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { ms, xs } from '../utils/Responsive'
import { primary, light, grey, gradientColor, buttonGradientColor } from '../utils/Color'
import { f, b } from '../utils/StyleHelper'
import Log from '../utils/Log'
import LinearGradient from 'react-native-linear-gradient';

const Button = ({
    disabled, block, children, color = primary,
    width, height, sm = false, lg = false,
    textColor = light, style, textStyle, gradasi = buttonGradientColor,
    ...props
}) => {
    const s = StyleSheet.create({
        container: {
            height: sm ? ms(25) : lg ? ms(44) : height ? ms(height) : ms(30),
            width: block ? '100%' : width ? ms(width) : null,
            justifyContent: 'center',
            alignItems: 'center',
        },
        fill: {
            backgroundColor: disabled ? grey : color
        },
        text: {
            color: textColor,
            fontSize: ms(lg ? 16 : 12),
        }
    })
    return (
        <TouchableOpacity
            disabled={disabled}
            style={[s.container, b.roundedLow, s.fill, style]}
            {...props}
        >
            <LinearGradient
                start={{x: 1, y: 0}} end={{x: 0, y: 0}}
                colors={disabled ? ['#828282', '#828282'] : gradasi}
                style={[s.container, b.roundedLow, s.fill, style]}
            >
                {typeof children == 'string' && <Text style={[s.text, f.gothamBold, textStyle]}>
                    {children}
                </Text>
                }
                {typeof children != 'string' && <>{children}</>}
            </LinearGradient>
        </TouchableOpacity>
    )
}

export default memo(Button)