import React, { memo } from 'react'
import { StyleSheet, View } from 'react-native'
import Ripple from 'react-native-material-ripple'
import { ms } from '../utils/Responsive'
import { b, p } from '../utils/StyleHelper'
import Log from '../utils/Log'
import RippleNative from './RippleNative'
import Badge from './Badge'
import { red } from '../utils/Color'
const RippleIcon = ({ margin, style, children, onPress, badgeCount = 0, ...props }) => {
    return (
        <View style={[margin]}>
            <RippleNative borderRadius={100}
                onPress={onPress}
                rippleFades={false}
                rippleCentered
                style={[b.p2, style]}
                {...props}
            >
                {children}
            </RippleNative>
            {badgeCount > 0 && <Badge title={badgeCount} color={red} sm style={s.badge} />}
        </View>
    )
}
const s = StyleSheet.create({
    badge: {
        position: 'absolute',
        right: 0
    }
})
export default memo(RippleIcon)