import React, { memo } from 'react'
import { View, StyleSheet } from 'react-native'
import { grey1 } from '../utils/Color'

const Hr = ({ style }) => {
    return (
        <View style={[s.hr, style]} />
    )
}
const s = StyleSheet.create({
    hr: {
        backgroundColor: grey1,
        width: '100%',
        height: 1
    },

})
export default memo(Hr)