import React, { Component } from 'react';
import { View, Text } from 'react-native'
import { p, b, c, f } from '../utils/StyleHelper';
import { ms } from '../utils/Responsive';
const Badge = ({ color, style, title, sm }) => (
    <View
        style={[p.row, b.roundedHigh, p.alignCenter, p.justifyCenter, { alignSelf: 'baseline', backgroundColor: color, height: ms(22), minWidth: ms(22) }, style]}>

        <Text style={[
            sm ? b.px1 : b.px16, c.light, f.openSansBold, f._12, p.textVerticalCenter
        ]}>
            {title}
        </Text>
    </View>
)
export default Badge