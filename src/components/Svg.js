import React, { memo } from 'react';
import { ms } from '../utils/Responsive';

import Profile from '../assets/icons/profile.svg';

//menu icon
import Home from '../assets/icons/home.svg';
import Favorite from '../assets/icons/favorite.svg';

// ILLUSTRATION
import IllustrationLogin from '../assets/images/illustration-2.svg'

// LOGO
import Logo from '../assets/icons/logo.svg'
import LogoSplash from '../assets/icons/logo-splash.svg'
import LogoGoogle from '../assets/icons/google.svg'
import LogoFacebook from '../assets/icons/facebook.svg'

const Svg = ({ icon, size = 30, fill, opacity, ...props }) => {
  const icons = {
    home: Home,
    favorite: Favorite,
    profile: Profile,
    // LOGO
    logo: Logo,
    logoSplash: LogoSplash,
    logoGoogle: LogoGoogle,
    logoFacebook: LogoFacebook,
    // ILLUSTRATION
    illustrationLogin: IllustrationLogin,

  };

  const Icon = icons[icon]
  return (
    <Icon
      width={ms(size)}
      fill={fill || null}
      height={ms(size)}
      fillOpacity={opacity || 1}
      {...props}
    />
  );
}
export default memo(Svg)
