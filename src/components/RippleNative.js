import React, { memo } from 'react'
import Ripple from 'react-native-material-ripple'
import { Platform, View } from 'react-native'
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import { xs } from '../utils/Responsive'
const RippleNative = ({
    borderRadius = 0, children, containerChildren,
    disabled, containerStyle, style, feedBackColor = "#aaa",
    ...props
}) => {
    return (
        Platform.OS == 'ios' ? <Ripple
            rippleContainerBorderRadius={xs(borderRadius)}
            container
            disabled={disabled}
            {...props} style={[style, containerStyle, { borderRadius: xs(borderRadius) }]}>{containerChildren}{children}</Ripple>
            :
            <View style={[containerStyle]}>
                {containerChildren}
                <View style={[{ borderRadius: xs(borderRadius), overflow: 'hidden' },]}>
                    <TouchableNativeFeedback
                        useForeground
                        background={TouchableNativeFeedback.Ripple(disabled ? 'transparent' : feedBackColor, true)}
                        disabled={disabled}
                        {...props} style={style}>
                        {children}
                    </TouchableNativeFeedback>
                </View>
            </View>
    )
}
export default RippleNative