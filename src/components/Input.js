import React, { useState, memo, useCallback } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { ms, xs, vs } from '../utils/Responsive'
import { b, c, f, p } from '../utils/StyleHelper'
import { secondary, light, primary, orange, red, grey, grey1, grey2, main } from '../utils/Color'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faUser, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import Log from '../utils/Log'

const Input = {
    Text: memo(({
        label, error, placeholder, icon, activeColor = main,
        onBlur = () => { }, onFocus = () => { },
        style, ...props
    }) => {
        const [focus, setFocus] = useState(false)
        const onFocusCallback = useCallback(() => {
            setFocus(true)
            onFocus()
        })
        const onBlurCallback = useCallback(() => {
            setFocus(false)
            onBlur()
        })

        const s = StyleSheet.create({
            textInput: {
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: '#fff',
            },
            focus: {
                borderColor: activeColor,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            notFocus: {
                borderColor: grey2,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            marginBottom: {
                marginBottom: xs(14)
            }
        })

        return (
            <View style={[style, s.marginBottom]}>
                <View style={[s.textInput, p.center, focus ? s.focus : s.notFocus]}>
                    <View style={[p.center, c.bgGrey1, {width: xs(36), height: xs(36), borderRadius: 100}]}>
                        <FontAwesomeIcon icon={icon ? icon : faUser} color={focus ? activeColor : light} size={ms(16)} />
                    </View>
                    <TextInput
                        autoCapitalize="none"
                        onBlur={onBlurCallback}
                        onFocus={onFocusCallback}
                        style={[b.container, c.grey, f._14, b.ml1]}
                        placeholder={placeholder ? placeholder : label}
                        placeholderTextColor={grey}
                        {...props}
                    />
                </View>
                <Error error={error} />
            </View>
        )
    }),
    TextArea: memo(({ label, error, style, ...props }) => {
        return (
            <View style={[b.mb16, style]}>
                <Label label={label} />
                <TextInput
                    multiline
                    autoCapitalize="none"
                    {...props}
                    style={[b.bordered, b.roundedLow, s.input, f.openSans, f._12, style]}
                />
                <Error error={error} />
            </View>
        )
    }),
    Password: memo(({
        label, error, placeholder, icon, activeColor = main,
        onBlur = () => { }, onFocus = () => { },
        style, ...props
    }) => {
        const [focus, setFocus] = useState(false);
        const [passwordVisible, setPasswordVisible] = useState(false);
        const onFocusCallback = useCallback(() => {
            setFocus(true)
            onFocus()
        })
        const onBlurCallback = useCallback(() => {
            setFocus(false)
            onBlur()
        })
        const s = StyleSheet.create({
            textInput: {
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: '#fff',
            },
            focus: {
                borderColor: activeColor,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            notFocus: {
                borderColor: grey2,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            marginBottom: {
                marginBottom: xs(14)
            }
        })
        console.log('render ',label, error)

        return (
            <View style={[style, s.marginBottom]}>
                <View style={[s.textInput, p.center, focus ? s.focus : s.notFocus]}>
                    <View style={[p.center, c.bgGrey1, {width: xs(36), height: xs(36), borderRadius: 100}]}>
                        <FontAwesomeIcon icon={icon ? icon : faUser} color={focus ? activeColor : light} size={ms(16)} />
                    </View>
                    <TextInput
                        autoCapitalize="none"
                        onBlur={onBlurCallback}
                        onFocus={onFocusCallback}
                        style={[b.container, c.grey, f._14, b.ml1]}
                        placeholder={placeholder ? placeholder : label}
                        secureTextEntry={!passwordVisible}
                        {...props}
                    />
                    <TouchableOpacity onPress={() => setPasswordVisible(!passwordVisible)} style={[s.icon, p.center]}>
                        <FontAwesomeIcon icon={passwordVisible ? faEyeSlash : faEye} color={grey} size={ms(18)} />
                    </TouchableOpacity>
                </View>
                <Error error={error} />
            </View>
        )
    }),
    PhoneNumber: memo(({
        label, error, placeholder, activeColor = red,
        onBlur = () => { }, onFocus = () => { },
        mb=14, style, ...props
    }) => {
        const [focus, setFocus] = useState(false)
        const onFocusCallback = useCallback(() => {
            setFocus(true)
            onFocus()
        })
        const onBlurCallback = useCallback(() => {
            setFocus(false)
            onBlur()
        })
        const s = StyleSheet.create({
            textInput: {
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: '#fff',
            },
            focus: {
                borderColor: activeColor,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            notFocus: {
                borderColor: grey2,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            code: {
                borderColor: grey2,
                borderWidth: 0,
                borderBottomWidth: 1,
            },
            marginBottom: {
                marginBottom: xs(mb)
            }
        })
        return (
            <View style={[style, s.marginBottom]}>
                <View style={[s.textInput, p.center]}>
                    <View style={[s.code]}>
                        <TextInput
                            style={[c.grey, f._24,]}
                            value="+62"
                            editable={false}
                        />
                    </View>
                    <View style={[b.ml2, b.container, focus ? s.focus : s.notFocus]}>
                        <TextInput
                            keyboardType={'phone-pad'}
                            onBlur={onBlurCallback}
                            onFocus={onFocusCallback}
                            style={[b.container, c.grey, f._24,]}
                            placeholder={placeholder ? placeholder : label}
                            placeholderTextColor={grey1}
                            {...props}
                        />
                    </View>
                    
                </View>
                <Error error={error} />
            </View>
        )
    })


}
const Label = memo(({ label }) => {
    return (
        <Text style={[s.label, label ? b.visible : b.hidden, c.secondary, f.openSans, f._12]}>{label}</Text>
    )
})

const Error = memo(({ error }) => {
    return (
        <Text style={[c.danger, f._12, f.openSans, error ? b.visible : b.hidden]}>{error}</Text>
    )
})
const s = StyleSheet.create({
    autoComplete: {
        position: 'relative',
        top: 0,
        left: 0,
        right: 0,
        paddingHorizontal: 0
    },
    autoCompleteList: {
        height: ms(46),
        borderBottomColor: grey2,
        borderBottomWidth: 1
    },
    autoCompleteListContainer: {
        maxHeight: vs(100),
        borderBottomRightRadius: xs(10),
        borderBottomLeftRadius: xs(10),
    },
    icon: {
        position: 'absolute',
        right: ms(8),
        top: 0,
        right: 0,
        height: ms(42),
        width: ms(40)
    },
    input: {
        paddingBottom: 0,
        minHeight: ms(42),
        paddingHorizontal: ms(16),
        color: secondary,
        textAlignVertical: "top"
    },
    label: {
        marginBottom: ms(7)
    },
})
export default Input