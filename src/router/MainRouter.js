import {
    createAppContainer,
    createSwitchNavigator,
} from 'react-navigation';

import AppRouter from './AppRouter';
//screen
import AuthIndex from '../modules/auth/screens/AuthIndex'
import Splash from '../screens/Splash';

//main switch router
const MainRouter = createSwitchNavigator({
    Splash,
    AuthIndex,
    AppRouter,
});
export default createAppContainer(MainRouter);
