import React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {scale as xs} from 'react-native-size-matters/extend';
// route
import HomeRouter from '../modules/home/HomeRouter';
import QRCodeRouter from '../modules/qrCode/QRCodeRouter';
import ProfileRouter from '../modules/profile/ProfileRouter';
// styles & fonts
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faUser, faQrcode} from '@fortawesome/free-solid-svg-icons';
// component
import Svg from '../components/Svg';
// utils
import {ms} from '../utils/Responsive';
import {p} from '../utils/StyleHelper';
import {main, grey1, grey2} from '../utils/Color';

//navigasi tab bawah
const AppRouter = createBottomTabNavigator(
  {
    Home: {
      screen: HomeRouter,
      navigationOptions: {
        tabBarIcon: ({focused}) => (
          <Svg icon="home" size={29} fill={focused ? main : grey1} />
        ),
      },
    },
    QrCode: {
      screen: QRCodeRouter,
      navigationOptions: {
        tabBarIcon: ({focused}) => (
          <View
            style={[
              p.center,
              {
                backgroundColor: main,
                marginBottom: 20,
                width: 50,
                height: 50,
                borderRadius: 100,
              },
            ]}>
            <FontAwesomeIcon icon={faQrcode} size={ms(35)} color={'white'} />
          </View>
        ),
        tabBarOptions: {
          style: {
            height: xs(70),
          },
          tabStyle: {
            paddingVertical: xs(8),
          },
          labelStyle: {
            fontSize: ms(10),
            fontFamily: 'OpenSans-Bold',
          },
          activeTintColor: main,
          inactiveTintColor: grey2,
          safeAreaInset: {
            bottom: 0,
            top: 0,
          },
        },
      },
    },
    Profile: {
      screen: ProfileRouter,
      navigationOptions: {
        tabBarIcon: ({focused}) => (
          <FontAwesomeIcon
            icon={faUser}
            size={ms(29)}
            color={focused ? main : grey1}
          />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      style: {
        height: xs(70),
      },
      tabStyle: {
        paddingVertical: xs(8),
      },
      labelStyle: {
        fontSize: ms(10),
        fontFamily: 'OpenSans-Bold',
      },
      activeTintColor: main,
      inactiveTintColor: grey2,
      safeAreaInset: {
        bottom: 0,
        top: 0,
      },
    },
    defaultNavigationOptions: ({navigation}) => {
      if (navigation.state.index > 0) {
        return {
          tabBarVisible: false,
        };
      }
      return {
        tabBarVisible: true,
      };
    },
  },
);

export default AppRouter;
