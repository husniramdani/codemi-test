import Axios from "axios"
import Config from '../../config'
import Log from '../utils/Log'
import { Store } from '../Store'

export const API = {
    call(endPoint, input = {}, config = { headers: {} }) {
        let state = Store.getState()
        // Log.debug("request service " + endPoint + " :", input);

        config.headers.service_name = endPoint;
        if (state.auth.token !== undefined && state.auth.token !== null) {
            config.headers.Authorization = `Bearer ${state.auth.token}`;
        }
        return Axios.post(Config.baseOnlineServiceUrl + endPoint, input, config)
            .then((response) => {
                Log.debug("response service " + endPoint + " :", response.data);
                return response.data;
            })
            .catch((error) => {
                Log.debug("response error service " + endPoint + " :", error.response);
                throw error.response.data;
            });
    },

    get(endPoint, input, config = { headers: {} }) {
        let state = Store.getState()
        Log.debug("request get '" + endPoint + "' :", input);

        if (state.auth.token !== undefined && state.auth.token !== null) {
            config.headers.Authorization = state.auth.token;
        }
        config.params = input
        return Axios.get(Config.baseOnlineServiceUrl + endPoint, config)
            .then((response) => {
                Log.debug("response get '" + endPoint + "' :", response.data);
                return response.data.data;
            })
            .catch((error) => {
                Log.debug("response error get '" + endPoint + "' :", error.response.data);
                throw error.response.data;
            });
    },

    post(endPoint, input, config = { headers: {} }) {
        let state = Store.getState()
        Log.debug("request post '" + endPoint + "' :", input);

        if (state.auth.token !== undefined && state.auth.token !== null) {
            config.headers.Authorization = state.auth.token;
        }
        return Axios.post(Config.baseOnlineServiceUrl + endPoint, input, config)
            .then((response) => {
                Log.debug("response post '" + endPoint + "' :", response.data);

                return response.data;
            })
            .catch((error) => {
                Log.debug("response error post '" + endPoint + "' :", error);
                throw error;
            });
    },

    put(endPoint, input, config = { headers: {} }) {
        let state = Store.getState()
        Log.debug("request put '" + endPoint + "' :", input);

        if (state.auth.token !== undefined && state.auth.token !== null) {
            config.headers.Authorization = state.auth.token;
        }
        return Axios.put(Config.baseServiceUrl + endPoint, input, config)
            .then((response) => {
                Log.debug("response put '" + endPoint + "' :", response.data);
                return response.data.data;
            })
            .catch((error) => {
                Log.debug("response error put '" + endPoint + "' :", error.response.data);
                throw error.response.data;
            });
    },
    ws(serviceName, input = {}, config = { headers: {} }) {
        let state = Store.getState()
        Log.debug("request ws " + serviceName + " :", input);

        // config.headers.service_name = serviceName;
        if (state.auth.token !== undefined && state.auth.token !== null) {
            config.headers.Authorization = state.auth.token;
        }
        return Axios.post(`${Config.wsServiceUrl}/${serviceName}`, input, config)
            .then((response) => {
                Log.debug("response service " + serviceName + " :", response.data);
                return response.data;
            })
            .catch((error) => {
                Log.debug("response error service " + serviceName + " :", error.response.data);
                throw error.response.data;
            });
    },

}
