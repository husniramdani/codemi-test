export const primary = "#4c7dfc"
export const btnPrimary = {
    backgroundColor: primary,
    color: "#fff"
}

export const secondary = "#898989"
export const btnSecondary = {
    backgroundColor: secondary,
    color: "#fff"
}

export const success = "#0cad71"
export const btnSuccess = {
    backgroundColor: success,
    color: "#fff"
}

export const warning = "#ec971f"
export const btnWarning = {
    backgroundColor: warning,
    color: "#fff"
}

export const danger = "rgb(253, 87, 118)"
export const btnDanger = {
    backgroundColor: danger,
    color: "#fff"
}

export const gradientColor = ['#FFFFFF', "#ECE9E6"]
export const headerGradientColor = ['#ECE9E6', "#BBD2C5"]
export const qrGradientColor = ['#00C9FF', "#92FE9D"]
export const buttonGradientColor = ['#20C271', "#20C271"]
export const light = "#fff"
export const black = "black"

export const grey = "grey"
export const grey1 = "#E4E4E4"
export const grey2 = "#6A636357"

export const blue = "blue"
export const blue3 = "#4267B2"

export const orange = "#FF7700"
export const orange1 = "#FF4E00"

export const red = "#FF4b5C"

export const main = "#00B14F"
export const main2 = "#5AE4A8"

export const homeBackground = "#3AB8AC"