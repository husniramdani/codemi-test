import Log from './Log';

var validateJs = require('validate.js')

const r = {
    required: {
        presence: {
            message: '^Kolom harap diisi'
        }
    },
    email: {
        email: {
            message: '^Email tidak valid'
        },
    },
    min: (val) => ({
        length: {
            minimum: val,
            message: `^Minimal ${val} huruf`
        }
    })
}
export const Rules = {
    phone_number: {
        ...r.required,
        ...r.min(8)
    },
    loginAuth: {
        ...r.required,
        ...r.min(3)
    },
    email: {
        ...r.required,
        ...r.email,
        ...r.min(3)
    },
    string: {
        ...r.required,
        ...r.min(3)
    }
}

const validateInput = (type, value) => {
    const result = validateJs(
        {
            [type]: value
        },
        {
            [type]: Rules[type]
        }
    );

    if (result) {
        return result[type][0];
    }

    return null;
}

export const validate = (input, rules) => {
    let keys = Object.keys(rules)
    let errors = {}
    keys.map(k => {
        let error = validateInput(rules[k], input[k] || null)
        if (error != null) errors[k] = error
    })
    return errors
}

export const mapErrorList = (error_list) => {
    let errors = {}
    let errorKeys = Object.keys(error_list)
    errorKeys.map(key => {
        errors[key] = error_list[key].message
    })
    return errors
}