import React from 'react'
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import { Text, View } from "react-native";
import { f, createShadow } from "./StyleHelper";
import { ms, xs } from "./Responsive";
import { darkBlue, light, windowsBlue } from "./Color";
import Log from './Log';

const labelStyle = (focused) => ([
    focused ? f.gothamBold : f.gotham, {
        fontSize: ms(14),
        lineHeight: ms(14),
        color: darkBlue,
        width: 200,
        textAlign: 'center'
    }
])
const createTab = (tabs, { containerStyle = {}, tabStyle = {}, swipeAble = true }) => {
    let data = {}
    let config = {
        swipeEnabled: swipeAble,
        // lazy: true,
        sceneContainerStyle: {
            ...containerStyle
        },
        tabBarOptions: {
            style: {
                backgroundColor: light,
                ...createShadow(3),
                ...tabStyle
            },
            indicatorStyle: {
                backgroundColor: windowsBlue,
                height: xs(3)
            },

            tabStyle: {
                marginLeft: 0,
                paddingLeft: 0,
                paddingRight: 0
            }
        },
    }
    tabs.map(({ label, screen }) => {
        data[label] = {
            screen,
            navigationOptions: {
                tabBarLabel: ({ focused }) => (
                    <Text style={labelStyle(focused)}>{label}</Text>
                )
            }
        }
    })
    return createMaterialTopTabNavigator(data, config)
}
export default createTab