import { combineReducers } from 'redux'
import AuthReducer from './modules/auth/AuthReducer'
import ConfigReducer from './modules/config/ConfigReducer'

const RootReducer = combineReducers({
    auth: AuthReducer,
    config: ConfigReducer
})

export default RootReducer