import {API} from '../../utils/Api';
import Log from '../../utils/Log';

function setLogin(results) {
  return {
    type: 'IS_LOGGED_IN',
    results,
  };
}
function setLogout() {
  return {
    type: 'IS_LOGGED_OUT',
  };
}
export function setNotNew() {
  return {
    type: 'IS_NOT_NEW',
  };
}
export function doLogin(payload = {}) {
  return (dispatch) => {
    return API.post('/auth/login', payload)
      .then((res) => {
        dispatch(setLogin(res));
        return res;
      })
      .catch((err) => {
        throw err;
      });
  };
}
export function doLogout() {
  return (dispatch) => {
    dispatch(setLogout());
    alert('Berhasil Logout');
  };
}
