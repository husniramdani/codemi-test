import React, {Component} from 'react';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView, View, Text, Image} from 'react-native';
// router
import {doLogin} from '../AuthAction';
// styles & icons
import {faUser, faLock} from '@fortawesome/free-solid-svg-icons';
// component
import Svg from '../../../components/Svg';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import RippleNative from '../../../components/RippleNative';
import RippleIcon from '../../../components/RippleIcon';
import ButtonGradation from '../../../components/ButtonGradation';
// utils
import {xs} from '../../../utils/Responsive';
import {Toast} from '../../../utils/AlertHelper';
import {gradientColor, light, blue3} from '../../../utils/Color';
import {c, b, p, f} from '../../../utils/StyleHelper';
import {validate, mapErrorList} from '../../../utils/Validator';

class AuthLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      input: {
        email: null,
        password: null,
      },
    };
  }

  // methods
  doLogin = () => {
    let errors = validate(this.state.input, {
      email: 'email',
      password: 'string',
    });
    this.setState({errors});
    if (Object.keys(errors).length > 0) {
      return;
    }

    const params = {
      email: this.state.input.email,
      password: this.state.input.password,
    };
    // redux action
    this.props
      .doLogin(params)
      .then((res) => {
        this.errors = {};
        // console.log('bisa', res);
        this.props.navigation.navigate('Home');
      })
      .catch((err) => {
        // console.log('err', err);
        if (err.error_list) {
          //map error validasi dari server
          let errors = mapErrorList(err.error_list);
          this.setState({errors});
        }
        if (err) {
          // pesan error
          alert(err);
        } else {
          Toast('koneksi bermasalah');
        }
      });
  };
  // goToRegister = () => this.props.navigation.navigate('Register');
  setInput = (val) => {
    this.setState((prevState) => ({
      input: {
        ...prevState.input,
        ...val,
      },
    }));
  };

  //avoid unnecessary rerendering
  setEmail = (email) => this.setInput({email});
  setPassword = (password) => this.setInput({password});

  render() {
    let {
      input: {email, password},
    } = this.state;
    const flagId = require('../../../assets/icons/flag-id.png');
    return (
      <>
        <LinearGradient colors={gradientColor} style={[b.container]}>
          <ScrollView style={[s.container]}>
            <View style={[p.alignEnd, b.mb1]}>
              <RippleNative
                style={[p.row, p.alignCenter]}
                feedBackColor="transparent"
                onPress={() => alert('Language IDN')}>
                <Text style={[b.mr1]}> IDN </Text>
                <Image
                  source={flagId}
                  style={{width: xs(25), height: xs(25)}}
                />
              </RippleNative>
            </View>
            <View style={[p.center]}>
              <Text style={[f._24, f.bold]}> Masuk </Text>
              <Svg height={145} icon="illustrationLogin" size={xs(260)} />
            </View>

            <View style={[b.p3]}>
              <View style={s.content}>
                <Input.Text
                  value={email}
                  label="E-mail"
                  onChangeText={this.setEmail}
                  error={this.state.errors.email}
                  icon={faUser}
                />
                <Input.Password
                  value={password}
                  label="Kata Sandi"
                  onChangeText={this.setPassword}
                  error={this.state.errors.password}
                  icon={faLock}
                />
                <Button
                  link
                  textStyle={[c.warning, f._14]}
                  onPress={() => alert('Forgot password not ready yet')}
                  style={[b.mb3]}>
                  Lupa kata sandi ?
                </Button>
                <ButtonGradation
                  style={[b.roundedHigh]}
                  lg
                  block
                  onPress={this.doLogin}>
                  Masuk
                </ButtonGradation>
              </View>
            </View>

            <View style={[p.center, b.mb3, p.row]}>
              <Text style={[f._14, f.thin]}>Belum punya akun ?</Text>
              <Button
                link
                textStyle={[f._16, f.bold]}
                onPress={() => alert('Not ready yet')}>
                Daftar
              </Button>
            </View>
            <View style={[p.center, b.mb3]}>
              <Text style={[f._14]}>─── atau menggunakan ───</Text>
            </View>
            <View style={[p.justifyCenter, p.row, {marginBottom: xs(56)}]}>
              <RippleIcon
                margin={b.mx2}
                style={[{backgroundColor: light}]}
                onPress={() => alert('Google Login not ready yet')}>
                <Svg icon="logoGoogle" />
              </RippleIcon>
              <RippleIcon
                margin={b.mx2}
                style={[{backgroundColor: blue3}]}
                onPress={() => alert('Facebook Login not ready yet')}>
                <Svg icon="logoFacebook" />
              </RippleIcon>
            </View>
          </ScrollView>
        </LinearGradient>
      </>
    );
  }
}
const s = {
  container: [b.container, b.p4],
  content: [c.bgLight, b.roundedHigh, b.p4, b.shadowLow],
};

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
  doLogin: (payload) => dispatch(doLogin(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AuthLogin);
