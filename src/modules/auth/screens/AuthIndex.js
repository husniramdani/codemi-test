import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
// route
import {doLogin} from '../AuthAction';
import AuthLogin from './AuthLogin';
// utils
import {xs, ms} from '../../../utils/Responsive';
import {c, b, p, f} from '../../../utils/StyleHelper';
import {darkBlue} from '../../../utils/Color';
import createTab from '../../../utils/ComponentHelper';

const AuthTab = createTab(
  [
    {
      label: 'Login',
      screen: AuthLogin,
    },
  ],
  {
    containerStyle: {
    },
    tabStyle: {
      display: 'none',
      marginHorizontal: xs(3),
    },
    swipeAble: false,
  },
);

class AuthIndex extends Component {
  static router = AuthTab.router;
  
  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.navigation.navigate('Home');
    }
  }

  render() {
    return (
      <AuthTab navigation={this.props.navigation} />
    );
  }
}

//styles
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: xs(27),
  },
  header: {
    marginTop: xs(45),
    height: xs(110),
    width: xs(204),
    marginBottom: xs(89),
  },
  adjustedLineHieght: {
    lineHeight: ms(27),
  },
  labelStyle: {
    fontSize: ms(14),
    lineHeight: ms(14),
    color: darkBlue,
  },
});
//combinedStyle
const s = {
  header: [styles.header, p.justifyBetween],
  labelStyle: (focused) => [
    focused ? f.gothamBold : f.gotham,
    styles.labelStyle,
  ],
  taglineText: [styles.adjustedLineHieght, f._18, f.gotham, c.light],
  taglineTitle: [, styles.adjustedLineHieght, f._24, f.gotham, c.light],
  textRegister: [f.openSans, f._14],
  textRegisterContainer: [p.row, p.alignCenter, b.mt2],
  title: [f._20, p.textCenter],
};

const mapStateToProps = (state) => ({
  isLoggedIn: state.auth.isLoggedIn,
  ...state.config,
});
const mapDispatchToProps = (dispatch) => ({
  doLogin: (payload) => dispatch(doLogin(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AuthIndex);
