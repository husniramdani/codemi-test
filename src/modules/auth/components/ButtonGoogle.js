import React, { memo } from "react";
import Button from "../../../components/Button";
import { StyleSheet, View, Text } from "react-native";
import { ms, xs } from "../../../utils/Responsive";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import { p, b, f, c } from "../../../utils/StyleHelper";
import { light, cloudyBlue } from "../../../utils/Color";

const ButtonGoogle = ({ label }) => (
    <Button block style={[s.buttonGoogle, p.row, p.justifyBetween, p.alignCenter, b.px3, b.rounded]} color={cloudyBlue}>
        <Text style={[f.gothamBold, f._14, c.light]}>{label}</Text>
        <FontAwesomeIcon icon={faChevronCircleRight} color={light} />
    </Button>
)
const s = StyleSheet.create({
    buttonGoogle: {
        height: ms(39),
        marginTop: xs(37),
        marginBottom: xs(86)
    }
})
export default memo(ButtonGoogle)