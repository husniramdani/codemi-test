const intitialState = {
  isLoggedIn: false,
  results: {},
  token: null,
  newUser: true,
};
const AuthReducer = (state = intitialState, action) => {
  switch (action.type) {
    case 'IS_LOGGED_IN':
      return {
        ...state,
        results: action.results.data,
        token: action.results.access_token,
        isLoggedIn: true,
      };
    case 'IS_LOGGED_OUT':
      return intitialState;
    case 'IS_NOT_NEW':
      return {
        ...state,
        newUser: false,
      };
    default:
      return state;
  }
};

export default AuthReducer;
