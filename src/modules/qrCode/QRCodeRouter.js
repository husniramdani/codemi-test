import React from 'react'
import { Text, Platform } from 'react-native'
import {
    createStackNavigator
} from 'react-navigation-stack'
import QRCodeIndex from './screens/QRCodeIndex'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronCircleLeft, faTimes } from '@fortawesome/free-solid-svg-icons'
import { xs, ms } from '../../utils/Responsive'
import { cloudyBlue, darkBlue } from '../../utils/Color'
import { createShadow, b } from '../../utils/StyleHelper'

const QRCodeRouter = createStackNavigator({
    QRCodeIndex: {
        screen: QRCodeIndex,
        navigationOptions: {
            //tanpa header
            headerShown: false
        }
    },
},
    {
        defaultNavigationOptions: {
            headerBackImage: () => <FontAwesomeIcon style={b.m1} icon={faChevronCircleLeft} size={ms(17)} color={cloudyBlue} />,
            headerBackTitle: null,
            headerLeftContainerStyle: {
                marginLeft: Platform.OS == 'ios' ? xs(25) : xs(-18 + 25),
            },
            safeAreaInsets: {
                top: 0,
                bottom: 0
            },
            headerStyle: {
                borderBottomWidth: 0,
                height: xs(64),
                ...createShadow(3)
            },
            headerTitleContainerStyle: {
                justifyContent: 'flex-start',
                alignContent: 'center',
                marginLeft: Platform.OS == 'ios' ? xs(-25) : xs(-15),
            },
            headerTitleStyle: {
                paddingTop: Platform.OS == 'ios' ? ms(5) : 0,
                fontFamily: "Gotham Bold",
                fontSize: ms(16),
                color: darkBlue,
            },
            headerRightContainerStyle: {
                marginRight: xs(25)
            }
        },
    })

export default QRCodeRouter