import React, {Component} from 'react';
import {View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {connect} from 'react-redux';
import QRCode from 'react-native-qrcode-svg';
// component
import Button from '../../../components/Button';
// utils
import {qrGradientColor} from '../../../utils/Color';
import {c, b, p, f} from '../../../utils/StyleHelper';

class QRCodeIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
    };
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  onSuccess = (e) => {
    this.setState({isModalVisible: false});
    alert('Hasil Scan QR Code : ' + e.data);
  };

  render() {
    return (
      <LinearGradient colors={qrGradientColor} style={[b.container]}>
        <View style={[b.container, p.center]}>
          <View
            style={[
              c.bgLight,
              b.roundedHigh,
              b.p4,
              b.shadowLow,
              p.alignSelfCenter,
            ]}>
            <QRCode
              size={160}
              value={this.props.results.username}
              // enableLinearGradient={true}
              linearGradient={['rgb(0, 201, 255)', 'rgb(146, 254, 157)']}
            />
          </View>
          <Button
            style={[b.roundedHigh, b.mt5, b.shadowHigh]}
            color={'white'}
            textStyle={[c.main, f._14]}
            lg
            onPress={this.toggleModal}>
            Scan QR Code
          </Button>
        </View>
        <Modal isVisible={this.state.isModalVisible}>
          <View style={[{flex: 1}, p.center]}>
            <QRCodeScanner
              cameraStyle={{
                height: 200,
                marginTop: 60,
                width: 300,
                alignSelf: 'center',
                justifyContent: 'center',
              }}
              onRead={this.onSuccess}
              flashMode={RNCamera.Constants.FlashMode.auto}
              topContent={
                <Text style={[f._18, c.light]}>Scanning Qr Code</Text>
              }
              bottomContent={
                <Button
                  style={[b.roundedHigh, b.mt5, b.shadowHigh]}
                  color={'white'}
                  textStyle={[c.main, f._14]}
                  lg
                  onPress={this.toggleModal}>
                  Close Camera
                </Button>
              }
            />
          </View>
        </Modal>
      </LinearGradient>
    );
  }
}

const mapStateToProps = (state) => ({
  results: state.auth.results,
});
export default connect(mapStateToProps, null)(QRCodeIndex);
