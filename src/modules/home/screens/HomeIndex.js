import React, {Component, memo, useCallback} from 'react';
import {View, Text, ScrollView, StyleSheet, ImageComponent} from 'react-native';
import {connect} from 'react-redux';
// styles & fonts
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faBell,
  faPlus,
  faMoneyBill,
  faPaperPlane,
  faGlobe,
  faWallet,
  faReceipt,
  faGamepad,
  faMobileAlt,
  faAngleDoubleRight
} from '@fortawesome/free-solid-svg-icons';
// component
import CarouselBanner from '../components/CarouselBanner';
import RippleNative from '../../../components/RippleNative';
// utils
import {c, b, p, f} from '../../../utils/StyleHelper';
import {xs, ms} from '../../../utils/Responsive';

const SLIDER_1_FIRST_ITEM = 1;

class HomeIndex extends Component {
  constructor(props) {
    super(props);
    this.sliderBannerRef = React.createRef();
    this.state = {
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
    };
  }

  onSnapToItem = (slider1ActiveSlide) => this.setState({slider1ActiveSlide});

  render() {
    return (
      <ScrollView
        contentContainerStyle={[b.h100, c.bgLight]}
        showsVerticalScrollIndicator={false}>
        <View style={{backgroundColor: '#f7f7f7'}}>
          <View style={[p.row, p.justifyBetween, c.light, b.px5, b.pt4]}>
            <Text style={[f.bold, f._18]}>
              Hai {this.props.results.username}
            </Text>
            <FontAwesomeIcon icon={faBell} size={ms(24)} color="#6A636357" />
          </View>
          <CarouselBanner
            sliderBannerRef={this.sliderBannerRef}
            slider1ActiveSlide={this.state.slider1ActiveSlide}
            onSnapToItem={this.onSnapToItem}
          />
        </View>
        <View style={[b.p4, b.pt3]}>
          <Text style={[f.bold, f._20]}>Features</Text>
        </View>
        <View style={[b.p4, b.pt0, p.justifyBetween, p.row]}>
          <FeaturesContent icon={faMoneyBill} label="Top up" />
          <FeaturesContent
            icon={faPaperPlane}
            label="Transfer"
            bgColor="#FFF9EC"
            color="#FFC861"
          />
          <FeaturesContent
            icon={faGlobe}
            label="Internet"
            bgColor="#E8FFF1"
            color="#3BC775"
          />
          <FeaturesContent
            icon={faWallet}
            label="Wallet"
            bgColor="#FFF1F0"
            color="#FF4133"
          />
        </View>
        <View style={[b.p4, b.pt0, p.justifyBetween, p.row]}>
          <FeaturesContent
            icon={faReceipt}
            label="Bill"
            bgColor="#FFF9EC"
            color="#FFC861"
          />
          <FeaturesContent
            icon={faGamepad}
            label="Games"
            bgColor="#E8FFF1"
            color="#3BC775"
          />
          <FeaturesContent
            icon={faMobileAlt}
            label="Prepaid"
            bgColor="#FFF1F0"
            color="#FF4133"
          />
          <FeaturesContent icon={faAngleDoubleRight} label="More" />
        </View>
      </ScrollView>
    );
  }
}

const FeaturesContent = memo(
  ({
    label = 'label',
    icon = faPlus,
    bgColor = '#F3EFFF',
    color = '#754ae8',
  }) => (
    <View style={[p.alignCenter]}>
      <RippleNative
        style={[
          {minWidth: xs(60), minHeight: xs(60), backgroundColor: bgColor},
          b.roundedHigh,
          p.center,
        ]}
        feedBackColor={'transparent'}
        onPress={() => console.log('features')}>
        <FontAwesomeIcon icon={icon} size={ms(28)} color={color} />
      </RippleNative>
      <Text style={[b.mt1, f._14]}>{label}</Text>
    </View>
  ),
);

const s = StyleSheet.create({
  bannerHome: {
    minHeight: xs(200),
  },
});

const mapStateToProps = (state) => ({
  results: state.auth.results,
  ...state.config,
});

export default connect(mapStateToProps, null)(HomeIndex);
