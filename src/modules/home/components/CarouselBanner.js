import React, {memo} from 'react';
import {View, StyleSheet} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import SliderEntry from './SliderEntry';
import {BANNER} from '../static/entries';
import {sliderWidth, itemWidth} from './SliderEntry.style';

const _renderItemWithParallax = ({item, index}) => {
    return (
        <SliderEntry
            data={item}
            even={(index + 1) % 2 === 0}
        />
    );
}
// console.log(BANNER)
const CarouselBanner = ({ sliderBannerRef, slider1ActiveSlide, onSnapToItem }) => (
  <View style={s.container}>
    <Carousel
      ref={sliderBannerRef}
      data={BANNER}
      renderItem={_renderItemWithParallax}
      sliderWidth={sliderWidth}
      itemWidth={itemWidth}
      hasParallaxImages={false}
      firstItem={1}
      inactiveSlideScale={0.94}
      inactiveSlideOpacity={0.7}
      // inactiveSlideShift={20}
      containerCustomStyle={s.slider}
      contentContainerCustomStyle={s.sliderContentContainer}
      loop={true}
      loopClonesPerSide={2}
      autoplay={true}
      autoplayDelay={500}
      autoplayInterval={3000}
      onSnapToItem={(index) => onSnapToItem(index)}
    />
    <Pagination
      dotsLength={BANNER.length}
      activeDotIndex={slider1ActiveSlide}
      containerStyle={s.paginationContainer}
      dotColor={"black"}
      dotStyle={s.paginationDot}
      inactiveDotColor="#1a1917"
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
      carouselRef={sliderBannerRef}
      tappableDots={!!sliderBannerRef}
    />
  </View>
);
const s = StyleSheet.create({
    scrollview: {
        flex: 1
    },
    container: {
        paddingBottom: 10,
    },
    containerDark: {
        backgroundColor: "#1a1917"
    },
    containerLight: {
        backgroundColor: 'white'
    },
    slider: {
        marginTop: 15,
        overflow: 'visible'
    },
    sliderContentContainer: {
        paddingVertical: 2
    },
    paginationContainer: {
        paddingVertical: 5
    },
    paginationDot: {
        width: 10,
        height: 10,
        borderRadius: 10,
        marginHorizontal: 1
    }
});
export default memo(CarouselBanner);
