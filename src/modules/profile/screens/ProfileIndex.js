import React, {Component, memo, useCallback} from 'react';
import {connect} from 'react-redux';
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
// redux
import {doLogout} from '../../auth/AuthAction';
// styles & fonts
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faPenSquare, faUser, faMobile, faEnvelope, faLock, faLanguage, faInfo, faSignOutAlt, faChevronRight} from '@fortawesome/free-solid-svg-icons';
// component
import RippleNative from '../../../components/RippleNative';
import Hr from '../../../components/Hr';
// utils
import {c, b, p, f} from '../../../utils/StyleHelper';
import {xs, ms, vs} from '../../../utils/Responsive';
import {headerGradientColor, light, grey2, grey1} from '../../../utils/Color';

class ProfileIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  doLogout = () => {
    this.props.navigation.navigate('AuthIndex');
    this.props.doLogout();
  }
  render() {
    const avatar = require('../../../assets/images/avatar.jpg');
    return (
      <ScrollView
        contentContainerStyle={[
          // {minHeight: 1000}
        ]}
        showsVerticalScrollIndicator={false}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={headerGradientColor}
          style={[
            s.headerProfile,
            b.roundedLowBottom,
            b.p5,
            p.row,
            p.justifyBetween,
          ]}>
          <View>
            <Text style={[f._24, b.mt2, f.bold]}>Profile Anda</Text>
            <RippleNative
              style={[p.row, p.alignCenter]}
              feedBackColor={grey2}
              onPress={() => console.log('Ganti Foto')}>
              {/* <Image source={flagId} style={{width: xs(25), height: xs(25)}} /> */}
              <FontAwesomeIcon icon={faPenSquare} size={ms(14)} />
              <Text style={[b.ml2]}>Ganti foto</Text>
            </RippleNative>
            <Text style={[f._18, b.mt2, f.bold]}>Halo {this.props.results.username}!</Text>
          </View>
          <Image style={[s.avatar, p.alignSelfCenter]} source={avatar} />
        </LinearGradient>
        <View style={[b.p4]}>
          <View style={[c.bgLight, b.rounded, b.p3, b.shadow, b.mb4]}>
            <Text style={[f._18, f.bold, b.mb2]}>Akun</Text>
            <ContentField label="Nama Lengkap" icon={faUser} value={this.props.results.username || "alex budiman"}/>
            <ContentField label="No. HP" icon={faMobile} value="+628923538485"/>
            <ContentField label="Email" icon={faEnvelope} value={this.props.results.email || "alex@mail.com"}/>
          </View>
          <View style={[c.bgLight, b.rounded, b.p3, b.shadow]}>
            <Text style={[f._18, f.bold, b.mb2]}>Lainnya</Text>
            <ContentField label="Kata Sandi" icon={faLock} value="**********"/>
            <ContentField label="Bahasa" icon={faLanguage} value="Indonesia"/>
            <ContentField label="Tentang" icon={faInfo}/>
            <ContentField label="Keluar" action={this.doLogout} icon={faSignOutAlt}/>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const ContentField = memo(({label="label", icon={faUser}, value, action = console.log("")}) => (
  <View style={value? [p.row, p.alignCenter, b.mb3] : [p.row, p.alignCenter, b.mb4]}>
    <View style={[s.iconBgRounder, c.bgMain, p.center, b.mr2]}>
      <FontAwesomeIcon icon={icon} size={ms(14)} color={light} />
    </View>
    <RippleNative
      style={[{width: xs(240)}]}
      feedBackColor={grey1}
      onPress={action}>
        <View style={[p.row, p.justifyBetween, p.alignCenter, b.mb1]}>
          <View>
            <Text style={[f._16]}>{label}</Text>
            <Text style={value? [c.grey] : [b.hidden]}>{value}</Text>
          </View>
          <FontAwesomeIcon icon={faChevronRight} size={ms(22)} color={grey2} />
        </View>
        <Hr style={value? [] : [b.hidden]}/>
    </RippleNative>
  </View>
));

const s = StyleSheet.create({
  headerProfile: {
    minHeight: xs(155),
  },
  avatar: {
    borderRadius: ms(100),
    height: ms(100),
    width: ms(100),
    borderWidth: xs(1),
  },
  iconBgRounder: {
    borderRadius: xs(30),
    width: xs(30),
    height: xs(30),
  },
});

const mapStateToProps = (state) => ({
  results: state.auth.results,
  ...state.config,
});
const mapDispatchToProps = (dispatch) => ({
  doLogout: (payload) => dispatch(doLogout(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ProfileIndex);
