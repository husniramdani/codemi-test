const intitialState = {
    config: {}
}
const ConfigReducer = (state = intitialState, action) => {
    switch (action.type) {
        case 'UPDATE_CONFIG':
            return {
                ...state,
                config: action.config
            }
        default:
            return state
    }
}

export default ConfigReducer