import { API } from "../../utils/Api"
import { Toast } from "../../utils/AlertHelper"

export function updateConfig() {
    return (dispatch) => {
        return API.call("FindConfig")
            .then(res => {
                dispatch({
                    type: 'UPDATE_CONFIG',
                    config: res
                })
            })
            .catch(err => {
                if (err.error_message) {
                    Toast(err.error_message)
                }
            })
    }
}